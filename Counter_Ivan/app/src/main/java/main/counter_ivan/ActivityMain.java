package main.counter_ivan;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityMain extends AppCompatActivity {
    TextView textView;
    Button buttonCount;
    Button buttonRestart;
    Button buttonToast;
    int contador = 0;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        textView = findViewById(R.id.viewContador);
        buttonCount = findViewById(R.id.buttonCount);
        buttonRestart = findViewById(R.id.buttonRestart);
        buttonToast = findViewById(R.id.buttonToast);
        buttonCount.setOnClickListener(new onClickListener());
        buttonRestart.setOnClickListener(new onClickListener1());
        buttonToast.setOnClickListener(new onClickListener2());
    }

    public void count(View view){
    textView.setText(Integer.toString(contador++));
    }
    private class onClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            count(v);
        }
    }
    public void resert(View view){
        textView.setText(Integer.toString(contador = 0));
    }
    private class onClickListener1 implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            resert(v);
        }
    }
    public void toast(View view){
        int duracion = Toast.LENGTH_LONG;
        String mensaje =getString(R.string.hellow_toast);
        Toast toast = Toast.makeText(this,mensaje,duracion);
        toast.show();
    }
    private class onClickListener2 implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            toast(v);
        }
    }
}

